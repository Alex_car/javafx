package calculator;

import addingMachine.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private TextField result;

    @FXML
    void clearAll(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();
    }

    @FXML
    void add(ActionEvent event) {
        result.clear();
        if (isEmptyAnimation(firstOperand)) {
            return;
        }
        if (isEmptyAnimation(secondOperand)) {
            return;
        }
        if (isValidation(firstOperand) == null || isValidation(secondOperand) == null) {
            return;
        }

        double result = getText(firstOperand) + getText(secondOperand);
        this.result.setText(String.valueOf(result));
    }

    @FXML
    void dev(ActionEvent event) {
        result.clear();
        if (isEmptyAnimation(firstOperand)) {
            return;
        }
        if (isEmptyAnimation(secondOperand)) {
            return;
        }

        double second = getText(secondOperand);
        if (second == 0) {
            getAnimation(secondOperand);
            result.setText("На 0 нельзя делить");
            return;
        }
        if (isValidation(firstOperand) == null || isValidation(secondOperand) == null) {
            return;
        }

        double result = getText(firstOperand) / second;
        this.result.setText(String.valueOf(result));
    }

    @FXML
    void multiply(ActionEvent event) {
        result.clear();
        if (isEmptyAnimation(firstOperand)) {
            return;
        }
        if (isEmptyAnimation(secondOperand)) {
            return;
        }
        if (isValidation(firstOperand) == null || isValidation(secondOperand) == null) {
            return;
        }

        double result = getText(firstOperand) * getText(secondOperand);
        this.result.setText(String.valueOf(result));
    }

    @FXML
    void sub(ActionEvent event) {
        result.clear();
        if (isEmptyAnimation(firstOperand)) {
            return;
        }
        if (isEmptyAnimation(secondOperand)) {
            return;
        }
        if (isValidation(firstOperand) == null || isValidation(secondOperand) == null) {
            return;
        }

        double result = getText(firstOperand) - getText(secondOperand);
        this.result.setText(String.valueOf(result));
    }

    /**
     * проверяет наполненность операнда
     *
     * @param operand введенный из приложения операнд
     * @return true если операнд пустой
     * false если не пустой
     */
    private boolean isEmptyAnimation(TextField operand) {
        if (operand.getText().isEmpty()) {
            getAnimation(operand);
            return true;
        }
        return false;
    }

    private Double isValidation(TextField operand) {
        Double temp = null;
        try {
            getText(operand);
        } catch (NumberFormatException e) {
            getAnimation(result);
            result.setText("Ошибка ввода данных");
        }
        return temp;
    }

    private double getText(TextField operand) {
        return Double.parseDouble(operand.getText());
    }

    private void getAnimation(TextField operand) {
        Shake operandAnim = new Shake(operand);
        operandAnim.playAnimation();
    }
}
