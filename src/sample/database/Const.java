package sample.database;

public class Const {
    public static final String USER_TABLE = "users";

    public static final String USERS_ID = "idUsers";
    public static final String USERS_FIRSTNAME = "Firstname";
    public static final String USERS_LASTNAME = "Lastname";
    public static final String USERS_USERNAME = "Username";
    public static final String USERS_PASSWORD = "Password";
    public static final String USERS_GENDER = "Gender";
}
