package sample.controller;


import java.io.IOException;
import java.sql.SQLException;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.database.DatabaseHandler;
import sample.database.User;

public class SignUpController {

    @FXML
    private Button signUpButton;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField surnameField;

    @FXML
    private ComboBox<?> gender;

    @FXML
    void initialize()  {
        signUpButton.setOnAction(event -> {
            try {
                signUpNewUser();

                signUpButton.getScene().getWindow().hide();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/sample/fxml/app.fxml"));
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Parent root = loader.getRoot();
                Stage stage = new Stage();
                stage.setScene(new Scene(root));
                stage.showAndWait();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    private void signUpNewUser() throws SQLException, ClassNotFoundException {
        DatabaseHandler dbHandler = new DatabaseHandler();
        String firstname = nameField.getText();
        String lastname = surnameField.getText();
        String username = loginField.getText();
        String password = passwordField.getText();
        String gender = this.gender.getValue().toString();

        User user = new User(firstname,lastname,username,password,gender);
        dbHandler.signUpUser(user);
    }
}
