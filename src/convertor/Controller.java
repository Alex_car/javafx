package convertor;

import addingMachine.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ComboBox<?> firstBox;

    @FXML
    private ComboBox<?> secondBox;

    @FXML
    private TextField operand;

    @FXML
    private TextField result;


    @FXML
    private Double checkFirstBox(ActionEvent event) {
            Double temp = getOperand(operand);
            if (firstBox.getValue() == null) {
                getAnimation(firstBox);
                return null;
            }
            if (temp != null) {
                if (firstBox.getValue().equals("Метр")) {
                    return temp * 100;
                }
                if (firstBox.getValue().equals("Километр")) {
                    return temp * 1000;
                }
                if (firstBox.getValue().equals("Дециметр")) {
                    return temp / 0.1;
                }
                return temp;
            }
        return null;
    }

    @FXML
    void checkSecondBox(ActionEvent event) {
        Double temp = checkFirstBox(event);
        if (temp != null) {
            if (secondBox.getValue().equals("Метр")) {
                temp /= 100;
            }
            if (secondBox.getValue().equals("Километр")) {
                temp /= 1000;
            }
            if (secondBox.getValue().equals("Дециметр")) {
                temp *= 0.1;
            }
            result.setText(String.valueOf(temp));
        }
    }

    private Double getOperand(TextField operand) {
        try {
            return Double.parseDouble(operand.getText());
        } catch (NumberFormatException e) {
            getAnimation(operand);
            result.setText("Неверное число");
        }
        return null;
    }

    private void getAnimation(Node node) {
        Shake animation = new Shake(node);
        animation.playAnimation();
    }

    @FXML
    void clearAll(ActionEvent event) {
        operand.clear();
        result.clear();
    }
}
