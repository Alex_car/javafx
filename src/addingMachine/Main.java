package addingMachine;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("addingMachine.fxml"));
        primaryStage.getIcons().add(new Image("addingMachine\\image.png"));
        primaryStage.setTitle("Складыватель");
        primaryStage.setScene(new Scene(root, 268, 283));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}