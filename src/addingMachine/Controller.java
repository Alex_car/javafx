package addingMachine;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

@SuppressWarnings("unused")
public class Controller {

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private TextField additionResult;

    @FXML
    void add(ActionEvent event) {
        try {
            if (firstOperand.getText().isEmpty()) {
                getAnimation(firstOperand);
                return;
            }
            if (secondOperand.getText().isEmpty()) {
                getAnimation(secondOperand);
                return;
            }

            int sum = getText(firstOperand) + getText(secondOperand);
            additionResult.setText(String.valueOf(sum));
        } catch (Exception e) {
            getAnimation(firstOperand);
            getAnimation(secondOperand);
            additionResult.setText("Ошибка");
        }
    }

    @FXML
    void clearAll(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        additionResult.clear();
    }

    private int getText(TextField operand) {
        return Integer.parseInt(operand.getText());
    }

    private void getAnimation(TextField operand) {
        Shake operandAnim = new Shake(operand);
        operandAnim.playAnimation();
    }
}
